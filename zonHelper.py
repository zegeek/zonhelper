import json
from zonhelper import *

spa_p1 = Member("23:20:25:56:aa:bb:cc:dd","SPA_P1")
spb_p1 = Member("23:20:25:56:aa:bb:cc:dd","SPB_P1")
vnx = Entity("VNX500_Lafayette")
vnx.add_wwn_member(spa_p1)
vnx.add_wwn_member(spb_p1)
vnx.set_mem_alias()
unity = Entity("Unity_Lafayette")
unity.add_wwn_member(spa_p1)
unity.add_wwn_member(spb_p1)
unity.set_mem_alias()

sess = Session("uib")
sess.get_host_index("VNX500_Lafayette")
print(sess.hosts[sess.get_host_index("VNX500_Lafayette")].toJSON())