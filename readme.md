## zonHelper, python utility for SAN zoning assistance
**Description:**
This a web application entended to help system administrator during zoning operation, especially at a project beginning. It allows the creation of entities that contains *WWN* members, and creating zones from the previously existing entities. No more copy past work and fastidious work.
**Contact:**
Contact the author for any further information: Contribution, Ideas, cloning request.
**Author:**
Belhassen Dahmen, contact: zegeek94 [at] gmail.com
**How to:**

 - Start by installing the package in your folder
 - Set a *virtualenv* with flask package
 - `set FLASK_APP=flaskr`
 - `flask run`

> Written with [StackEdit](https://stackedit.io/).