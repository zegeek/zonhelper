from flask import (Blueprint, redirect,flash, g, render_template, request, session, url_for)
from .zonhelper import Session,Entity,Member

bp = Blueprint('sess', __name__, url_prefix='/sess')

@bp.route('/start',methods=['POST'])
def start():
    if request.method == 'POST':
        session_name = request.form['session-name']
        session['session_name'] = session_name
        return redirect(url_for('sess.run',sessionName = session_name))
    else:
        return redirect(url_for('index'))

@bp.route('/run/<sessionName>')
def run(sessionName):
    ss = Session.Session(sessionName)
    g.ss = ss
    return render_template('session.html', storages=ss.get_storages(),hosts=ss.get_hosts())

@bp.route('/add/<entityType>',methods=['POST'])
def add(entityType):
    if request.method == 'POST':
        session_name = session['session_name']
        ss = Session.Session(session_name)
        print(request.get_json(force=True))
        e = Entity.Entity.fromJSON(request.get_json(force=True))
        e.set_mem_alias()
        if entityType == 's':
            ss.add_storage(e)
            ss.dump_storages()
        elif entityType == 'h':
            ss.add_host(e)
            ss.dump_hosts()
        return url_for('sess.run',sessionName = session_name)
    else:
        return url_for('index')
@bp.route('/del/<entityType>',methods=['POST'])
def delete(entityType):
    if request.method == 'POST':
        session_name = session['session_name']
        ss = Session.Session(session_name)
        mem = request.get_json(force=True)
        entity = mem['entity']
        if 'member' in mem:
            member = mem['member']
            if entityType == 'h':
                ss.hosts[ss.get_host_index(entity)].del_wwn_member(member)
                ss.dump_hosts()
            elif entityType == 's':
                ss.storages[ss.get_storage_index(entity)].del_wwn_member(member)
                ss.dump_storages()
        else:
            if entityType == 'h':
                ss.delete_host(entity)
                ss.dump_hosts()
            elif entityType == 's':
                ss.delete_storage(entity)
                ss.dump_storages()
        return url_for('sess.run',sessionName=session_name)
    else:
        return url_for('index')

@bp.route('/get/<entityType>',methods=['POST'])
def get(entityType):
    if request.method == 'POST':
        session_name = session['session_name']
        ss = Session.Session(session_name)
        mem = request.get_json(force=True)
        if entityType == 'h':
            e = ss.hosts[ss.get_host_index(mem['name'])]
        elif entityType == 's':
            e = ss.storages[ss.get_storage_index(mem['name'])]
        return e.toJSON()
    else:
        return "{}"

@bp.route('/edit/<entityType>',methods=['POST'])
def edit(entityType):
    session_name = session['session_name']
    ss = Session.Session(session_name)
    mem = request.get_json(force=True)
    e = Entity.Entity.fromJSON(mem['entity'])
    e.set_mem_alias()
    if entityType == 'h':
        ss.hosts[ss.get_host_index(mem['oldname'])] = e
        ss.dump_hosts()
    elif entityType == 's':
        ss.storages[ss.get_storage_index(mem['oldname'])] = e
        ss.dump_storages()
    return url_for('sess.run',sessionName=session_name)

@bp.route('/cap') 
def cap():
    session_name = session['session_name']
    ss = Session.Session(session_name)
    ss.capitalize_names()
    return redirect(url_for('sess.run',sessionName=session_name))
@bp.route('/script',methods=['POST'])
def script():
    if 'session_name' in session:
        s = Session.Session(session['session_name'])
        data = request.get_json(force=True)
        ss = ""
        if s.get_host_index(data['name']) != -1:
            for m in s.hosts[s.get_host_index(data['name'])].get_all_wwn_member():
                d_a = s.__class__.cisco['alias'].format(m.get_alias(),m.get_wwn())
                ss += d_a+"<br>"
        elif s.get_storage_index(data['name']) != -1:
            for m in s.storages[s.get_storage_index(data['name'])].get_all_wwn_member():
                d_a = s.__class__.cisco['alias'].format(m.get_alias(),m.get_wwn())
                ss += d_a+"<br>"
        return ss
@bp.route('/script/<entityType>', methods=['POST'])
def all_script(entityType):
    if 'session_name' in session:
        s = Session.Session(session['session_name'])
        ss = ""
        if entityType == 'h':
            for h in s.get_hosts():
                for m in h.get_all_wwn_member():
                    d_a = s.__class__.cisco['alias'].format(m.get_alias(),m.get_wwn())
                    ss += d_a+"<br>"
        elif entityType == 's':
            for st in s.get_storages():
                for m in st.get_all_wwn_member():
                    d_a = s.__class__.cisco['alias'].format(m.get_alias(),m.get_wwn())
                    ss += d_a+"<br>"
        return ss