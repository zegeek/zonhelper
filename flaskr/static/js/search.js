$(document).ready(function() {
    $('#search-storage').keyup(function() {
        var val = $(this).val();
        var name ="";
        $('.storage').each(function(){
            if(val.length > 0)
            {
                name = $(this).find('h5').html();
                if(name.toUpperCase().indexOf(val.toUpperCase()) > -1)
                {
                    $(this).show();
                }
                else
                {
                    $(this).hide();
                }
            }
            else
            {
                $(this).show();
            }
        })
    })
    $('#search-host').keyup(function() {
        var val = $(this).val();
        var name ="";
        $('.host').each(function(){
            if(val.length > 0)
            {
                name = $(this).find('h5').html();
                if(name.toUpperCase().indexOf(val.toUpperCase()) > -1)
                {
                    $(this).show();
                }
                else
                {
                    $(this).hide();
                }
            }
            else
            {
                $(this).show();
            }
        })
    })
    $('#search-zone').keyup(function() {
        var val = $(this).val();
        var name ="";
        $('.zone').each(function(){
            if(val.length > 0)
            {
                name = $(this).find('h5').html();
                if(name.toUpperCase().indexOf(val.toUpperCase()) > -1)
                {
                    $(this).show();
                }
                else
                {
                    $(this).hide();
                }
            }
            else
            {
                $(this).show();
            }
        })
    })
});