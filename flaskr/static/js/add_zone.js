function update_entity_list()
{
    var type = $('#entityType').val();
    $.post('/zon/get/'+type,function(d){
        var e_l = JSON.parse(d);
        $('#entityName').html("");
        e_l.forEach(element => {
            $('#entityName').append($('<option>',{
                value: element.index,
                text: element.name
            }));
        });
    });
    update_member_list();
}
function update_member_list()
{
    var type = $('#entityType').val();
    var entityName = $('#entityName').val();
    var data = {
        'index':entityName
    };
    $.post('/zon/ent/'+type,JSON.stringify(data),function(d)
    {
        var members = JSON.parse(d);
        $('#memberName').html("");
        members.forEach(element => {
            $('#memberName').append($('<option>', {
                value: element.value,
                text : element.name
            }));
        });
    });

}
$(document).ready(function(){
    $('#entityType').change(function() {
        update_entity_list();
    });
    $('#entityName').change(function() {
        update_member_list();
    })
    $('#add-zone-modal').on('shown.bs.modal',function(mod){
        update_entity_list();
    });
    $('#add-zone-modal').on('hidden.bs.modal',function(mod){
        $('#zone-members').html("");
    });
    $('.add-zone-member').click(function(){
        var value = $('#memberName').val();
        var entityName = $('#entityName option:selected').text();
        var memberName = $('#memberName option:selected').text();
        var member = "<div class=\"row justify-content-center\">\
            <input type=\"hidden\" value=\""+value+"\" class=\"zone-member\">\
            <div class=\"col-md-7 text-right\">\
                <h5 class=\"align-middle\">"+entityName+":"+memberName+"</h5>\
            </div>\
            <div class=\"col-md-5 \">\
                <a role=\"button\" class=\"btn btn-light del-zone-mem\" data-target=\""+value+"\"><i class=\"fas fa-trash-alt\"></i></a>\
            </div>\
        </div>"
        $('#zone-members').append(member);
    });
    $('#add-zone').submit(function(e) {
        e.preventDefault();
        var dataList = [];
        $(this).find('.zone-member').each(function(index){
            dataList.push($(this).val());
        });
        if(dataList.length >= 2)
        {
            $.post('/zon/add',JSON.stringify(dataList),function(d) {
                $(location).attr('href',d);
            });
        }
    });
    $('.del-zone').click(function(){
        var zoneIndex = $(this).data('target');
        $.post('/zon/del/'+zoneIndex,function(d){
            $(location).attr('href',d)
        })
    })
    $('.script-zone').click(function() {
        var script = $(this).data('target');
        $('#script-zone-display').html($('#'+script).val());
        $('#show-zone-script').modal('show');
    })
    $('.script-zone-all').click(function() {
        var script = "";
        $('.zone:visible').each(function(){
            script += $(this).find('.zone-script').val();
            script += "<br>"
        })
        $('#script-zone-display').html(script);
        $('#show-zone-script').modal('show');
    })
});