$(document).ready(function(){
    $('.del').click(function() {
        var entity_type = $(this).data('type');
        var entity = $(this).data('target');
        if($(this).data('member').length > 0)
            var member = $(this).data('member');
        var json = 
        {
            'entity':entity,
            'member': member
        };
        $.post("/sess/del/"+entity_type,JSON.stringify(json),function(d){
            $(location).attr('href',d);
        })
    });
});