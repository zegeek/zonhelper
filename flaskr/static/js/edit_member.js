function deleteMemberEdit(member)
{
    var edit_modal = $('#edit-entity-modal');
    edit_modal.find('div#member-'+member).remove();
    reorder();
}
$(document).ready(function() {
    $('.edi').click(function(){
        var edit_modal = $('#edit-entity-modal');
        var entityType = $(this).data('type');
        var entity = $(this).data('target');
        var data = {
            'name':entity
        }
        var ent = null;
        $.post('/sess/get/'+entityType,JSON.stringify(data),function(d) {
            ent = JSON.parse(d);
            edit_modal.find('#entity-type-edit').val(entityType);
            edit_modal.find('#old-name').val(ent['name']);
            edit_modal.find('#entity-name-edit').val(ent['name']);
            edit_modal.find('#members').html("");
            ent['wwnMem'].forEach((mem,index) =>{
                var element = "<div class=\"form-row mb-1 member\" id=\"member-"+(index+1)+"\">\
                    <div class=\"col\">\
                    <input type=\"text\" class=\"form-control member-name\" name=\"member-"+(index+1)+"-name\" placeholder=\"Name\" value=\""+mem['name']+"\" required>\
                    </div>\
                    <div class=\"col\">\
                    <input type=\"text\" class=\"form-control member-wwn\" name=\"member-"+(index+1)+"-wwn\" placeholder=\"PWWN\" value=\""+mem['wwn']+"\" required>\
                    </div>\
                </div>";
                edit_modal.find('#members').append(element);
            });
            edit_modal.modal('toggle');
        })
    });
    $('#add-member-edit').click(function() {
        var edit_modal = $('#edit-entity-modal');
        var count = $('#edit-entity-modal #members > div').length;
        var element = "<div class=\"form-row mb-1 member\" id=\"member-"+(count+1)+"\">\
            <div class=\"col-md-3\">\
            <input type=\"text\" class=\"form-control member-name\" name=\"member-"+(count+1)+"-name\" placeholder=\"Name\" required>\
            </div>\
            <div class=\"col-md-7\">\
            <input type=\"text\" class=\"form-control member-wwn\" name=\"member-"+(count+1)+"-wwn\" placeholder=\"PWWN\" required>\
            </div>\
            <div class=\"col-md-2\">\
            <input type=\"button\" class=\"btn btn-secondary\" value=\"Delete\" onclick=\"deleteMemberEdit("+(count+1)+")\">\
            <div>\
        </div>";
        edit_modal.find('#members').append(element);
    });
    $('#edit-entity').submit(function(e) {
        e.preventDefault();
        var edit_modal = $('#edit-entity-modal');
        var members = [];
        var entityType = edit_modal.find('#entity-type-edit').val();
        var oldName = edit_modal.find('#old-name').val();
        if(edit_modal.find('#members > .member').length > 0)
        {
            edit_modal.find('#members').children('.member').each(function() {
                var member = {
                    'wwn' : $(this).find('.member-wwn').val(),
                    'name': $(this).find('.member-name').val(),
                    'alias': ''
                };
                members.push(member);
            });
        }
        var entity = 
        {
            'name' : $(this).find('#entity-name-edit').val(),
            'wwnMem': members
        };
        var data =
        {
            'oldname':oldName,
            'entity':entity
        }
        var toSend = JSON.stringify(data);
        $.post('/sess/edit/'+entityType,toSend,function(d)
        {
            $(location).attr('href',d)
        })
    })
    $('.script-entity').click(function(){
        var entityName = $(this).data('target');
        $.post('/sess/script',JSON.stringify({ 'name':entityName}),function(d){
            $('#script-entity-display').html(d);
            $('#show-entity-script').modal('show');
        })
    });
    $('.script-s-entity').click(function(){
        $.post('/sess/script/s',function(d){
            $('#script-entity-display').html(d);
            $('#show-entity-script').modal('show');
        })
    });
    $('.script-h-entity').click(function(){
        $.post('/sess/script/h',function(d){
            $('#script-entity-display').html(d);
            $('#show-entity-script').modal('show');
        })
    });
});