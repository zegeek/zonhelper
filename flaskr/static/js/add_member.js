function reorder()
{
    var count = $('#members > div').length;
    $('#members > div').each(function(index)
    {
        $(this).attr('id','member-'+(index+1));
        $(this).find('.member-name').attr('name','member-'+(index+1)+'-name');
        $(this).find('.member-wwn').attr('name','member-'+(index+1)+'-wwn');
        $(this).find('[type=button]').attr('onclick','deleteMember('+(index+1)+')');
    });
}
function deleteMember(member)
{
    $('div#member-'+member).remove();
    reorder();
}
$('document').ready(function(){
    $('#add-member').click(function() {
        var count = $('#members > div').length;
        var element = "<div class=\"form-row mb-1 member\" id=\"member-"+(count+1)+"\">\
            <div class=\"col-md-3\">\
            <input type=\"text\" class=\"form-control member-name\" name=\"member-"+(count+1)+"-name\" placeholder=\"Name\" required>\
            </div>\
            <div class=\"col-md-7\">\
            <input type=\"text\" class=\"form-control member-wwn\" name=\"member-"+(count+1)+"-wwn\" placeholder=\"PWWN\" required>\
            </div>\
            <div class=\"col-md-2\">\
            <input type=\"button\" class=\"btn btn-secondary\" value=\"Delete\" onclick=\"deleteMember("+(count+1)+")\">\
            <div>\
        </div>";
        $('#members').append(element);
    });
    $('#add-entity').submit(function(e){
        e.preventDefault();
        var members = [];
        var entityType = $('#entity-type').val();
        if($('#members > .member').length > 0)
        {
            $('#members').children('.member').each(function() {
                var member = {
                    'wwn' : $(this).find('.member-wwn').val(),
                    'name': $(this).find('.member-name').val(),
                    'alias': ''
                };
                members.push(member);
            });
        }
        var entity = 
        {
            'name' : $('#entity-name').val(),
            'wwnMem': members
        };
        var data = JSON.stringify(entity);
        console.log(data);
        $.post("/sess/add/"+entityType,data,function(d){
            $(location).attr('href',d);
        });
    });
});