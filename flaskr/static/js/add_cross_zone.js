function c_update_entity_list()
{
    var type = $('#entityType-c').val();
    $.post('/zon/get/'+type,function(d){
        var e_l = JSON.parse(d);
        $('#entityName-c').html("");
        e_l.forEach(element => {
            $('#entityName-c').append($('<option>',{
                value: element.index,
                text: element.name
            }));
        });
    });
    c_update_member_list();
}
function c_update_member_list()
{
    var type = $('#entityType-c').val();
    var entityName = $('#entityName-c').val();
    var data = {
        'index':entityName
    };
    if(type == "h")
    {
        $('#memberName-c').html("").append($('<option>', {
            value: 'h.'+entityName+'.odd',
            text : 'Odd'
        })).append($('<option>', {
            value: 'h.'+entityName+'.even',
            text : 'Even'
        }));
        /*$.post('/zon/ent/'+type,JSON.stringify(data),function(d)
        {
            var members = JSON.parse(d);
            $('#memberName-c').html("");
            members.forEach(element => {
                $('#memberName-c').append($('<option>', {
                    value: element.value,
                    text : element.name
                }));
            });
        });*/
    }
    else if(type == "s")
    {
        $('#memberName-c').html("").append($('<option>', {
            value: 's.'+entityName+'.odd',
            text : 'Odd'
        })).append($('<option>', {
            value: 's.'+entityName+'.even',
            text : 'Even'
        }));
    }
}
$(document).ready(function(){
    $('#entityType-c').change(function() {
        c_update_entity_list();
    });
    $('#entityName-c').change(function() {
        c_update_member_list();
    })
    $('#add-crosszone-modal').on('shown.bs.modal',function(mod){
        c_update_entity_list();
    });
    $('#add-crosszone-modal').on('hidden.bs.modal',function(mod){
        $('#zone-members-c').html("");
    });
    $('.add-zone-member-c').click(function(){
        var value = $('#memberName-c').val();
        var entityName = $('#entityName-c option:selected').text();
        var memberName = $('#memberName-c option:selected').text();
        var member = "<div class=\"row justify-content-center\">\
            <input type=\"hidden\" value=\""+value+"\" class=\"zone-member\">\
            <div class=\"col-md-7 text-right\">\
                <h5 class=\"align-middle\">"+entityName+":"+memberName+"</h5>\
            </div>\
            <div class=\"col-md-5 \">\
                <a role=\"button\" class=\"btn btn-light del-zone-mem\" data-target=\""+value+"\"><i class=\"fas fa-trash-alt\"></i></a>\
            </div>\
        </div>"
        $('#zone-members-c').append(member);
    });
    $('#add-crosszone').submit(function(e) {
        e.preventDefault();
        var dataList = [];
        $(this).find('.zone-member').each(function(index){
            dataList.push($(this).val());
        });
        if(dataList.length >= 2)
        {
            $.post('/zon/addcross',JSON.stringify(dataList),function(d) {
                $(location).attr('href',d);
            });
        }
    });
});