from .JsonSerializable import JsonSerializable

class Member(JsonSerializable):
    def __init__(self,wwn,name,alias=""):
        self.wwn = wwn
        self.alias = alias
        self.name = name
    def set_wwn(self,wwn):
        verif = wwn.count(":")
        if verif == 7:
            self.wwn = wwn
        else:
            raise ValueError('Invalid WWN')
    def get_wwn(self):
        return self.wwn
    def set_name(self,name):
        self.name = name
    def get_name(self):
        return self.name
    def set_alias(self,alias):
        self.alias = alias
    def get_alias(self):
        return self.alias
    @classmethod
    def fromJSON(cls, data):
        return cls(**data)
