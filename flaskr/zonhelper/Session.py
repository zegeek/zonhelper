import os
import json
from .Entity import Entity
from .Zone import Zone

class Session:
    session_dir = "sessions"
    hosts_file = session_dir+"/{}/hosts.json"
    storages_file = session_dir+"/{}/storages.json"
    zones_file = session_dir+"/{}/zones.json"
    cisco = {
        "zone":"zone name {}<br>\
member device-alias {}<br>\
member device-alias {}",
        "alias": "device-alias name {} pwwn {}"
    }
    def __init__(self,name):
        self.name = name
        self.hosts = []
        self.storages = []
        self.zones = []
        try:
            os.makedirs("{}/{}".format(self.__class__.session_dir,self.name))
        except FileExistsError:
            if not(os.path.exists(self.__class__.hosts_file.format(self.name)) and os.path.exists(self.__class__.storages_file.format(self.name)) and os.path.exists(self.__class__.zones_file.format(self.name))):
                print("[Warning] {} already exists, but database files are missing.".format(name))
            try:
                with open(self.__class__.hosts_file.format(self.name)) as json_file:
                    hosts_json = json.load(json_file)
                    for h in hosts_json:
                        self.hosts.append(Entity.fromJSON(h))
            except FileNotFoundError:
                print("[Warning] {} not yet created.".format(self.__class__.hosts_file.format(self.name)))
            try:
                with open(self.__class__.storages_file.format(self.name)) as json_file:
                    storages_json = json.load(json_file)
                    for s in storages_json:
                        self.storages.append(Entity.fromJSON(s))
            except FileNotFoundError:
                print("[Warning] {} not yet created.".format(self.__class__.storages_file.format(self.name)))
            try:
                with open(self.__class__.zones_file.format(self.name)) as json_file:
                    zones_json = json.load(json_file)
                    for z in zones_json:
                        self.zones.append(Zone.fromJSON(z))
            except FileNotFoundError:
                print("[Warning] {} not yet created.".format(self.__class__.zones_file.format(self.name)))
    def capitalize_names(self):
        for h in self.hosts:
            h.name = h.name.upper()
            for m in h.wwnMem:
                m.name = m.name.upper()
            h.set_mem_alias()
        for s in self.storages:
            s.name = s.name.upper()
            for m in s.wwnMem:
                m.name = m.name.upper()
            s.set_mem_alias()
        self.dump_hosts()
        self.dump_storages()

    def get_hosts(self):
        return self.hosts
    def get_storages(self):
        return self.storages
    def get_zones(self):
        return self.zones
    def add_host(self,host):
        self.hosts.append(host)
    def delete_host(self,name):
        deleted = False
        index = 0
        while not(deleted) and index < len(self.hosts):
            if self.hosts[index].name == name:
                self.hosts.pop(index)
                deleted = True
            else:
                index += 1
    def get_host_index(self,name):
        found = False
        index = 0
        while not(found) and index < len(self.hosts):
            if self.hosts[index].name == name:
                found = True
            else:
                index += 1
        if found:
            return index
        else:
            return -1
    def add_storage(self,storage):
        self.storages.append(storage)
    def delete_storage(self,name):
        deleted = False
        index = 0
        while not(deleted) and index < len(self.storages):
            if self.storages[index].name == name:
                self.storages.pop(index)
                deleted = True
            else:
                index += 1
    def get_storage_index(self,name):
        found = False
        index = 0
        while not(found) and index < len(self.storages):
            if self.storages[index].name == name:
                found = True
            else:
                index += 1
        if found:
            return index
        else:
            return -1
    def create_zone(self, members = []):
        z = Zone(members)
        if len(z.zoneMem) >= 2:
            aliases = []
            for i in range(2):
                if z.get_entity_type(i) == 'h':
                    aliases.append("{}".format(self.hosts[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_alias()))
                elif z.get_entity_type(i) == 's':
                    aliases.append("{}".format(self.storages[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_alias()))
            z.set_zone_name(aliases)
        self.zones.append(z)
    def delete_zone(self, index):
        if len(self.zones) > 1:
            self.zones.pop(index)
        else:
            self.zones = []

    def dump_hosts(self):
        index = 0
        with open(self.__class__.hosts_file.format(self.name),'w') as out_file:
            ss = "["
            for h in self.hosts:
                ss += h.toJSON()
                if index < len(self.hosts)-1:
                    ss += ","
                index += 1
            ss += "]"
            out_file.write(ss)
    def dump_storages(self):
        index = 0
        with open(self.__class__.storages_file.format(self.name),'w') as out_file:
            ss = "["
            for s in self.storages:
                ss += s.toJSON()
                if index < len(self.storages)-1:
                    ss += ","
                index += 1
            ss += "]"
            out_file.write(ss)
    def dump_zones(self):
        index = 0
        with open(self.__class__.zones_file.format(self.name),'w') as out_file:
            ss = "["
            for z in self.zones:
                ss += z.toJSON()
                if index < len(self.zones)-1:
                    ss += ","
                index += 1
            ss += "]"
            out_file.write(ss)
    def dump_db(self):
        self.dump_hosts()
        self.dump_storages()
        self.dump_zones()