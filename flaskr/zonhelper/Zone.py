from .JsonSerializable import JsonSerializable

#for zone
#member is a string withformat <entityType>.<entityIndex>.<memberIndex>

class Zone(JsonSerializable):
    separator = '.'
    entityTypeIndex = 0
    entityIndex = 1
    memIndex = 2
    def __init__(self,zoneMem = [],name = ""):
        self.name = name
        self.zoneMem = zoneMem
    def set_zone_name(self,nameL=[]):
        if len(nameL) > 1:
            self.name = "{}_{}".format(nameL[0],nameL[1])
        else:
            self.name = "{}_Zone".format(nameL[0])
    def add_member(self,mem):
        self.zoneMem.append(mem)
    def get_entity_type(self,memberIndex):
        li = self.zoneMem[memberIndex].split(self.__class__.separator)
        return li[self.__class__.entityTypeIndex]
    def get_entity_index(self,memberIndex):
        li = self.zoneMem[memberIndex].split(self.__class__.separator)
        return int(li[self.__class__.entityIndex])
    def get_member_index(self,memberIndex):
        li = self.zoneMem[memberIndex].split(self.__class__.separator)
        return int(li[self.__class__.memIndex])

    @classmethod
    def fromJSON(cls, data):
        return cls(data["zoneMem"],data["name"])