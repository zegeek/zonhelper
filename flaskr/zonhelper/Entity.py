from .JsonSerializable import JsonSerializable
from typing import List
from .Member import Member


class Entity(JsonSerializable):
    def __init__(self,name):
        self.name = name
        self.wwnMem = []
    def add_wwn_member(self, wwnMember):
        self.wwnMem.append(wwnMember)
    def get_wwn_member(self,criteria=""):
        found = False
        index = 0
        while not(found) and index < len(self.wwnMem):
            if criteria.count(":") == 7:
                if self.wwnMem[index].get_wwn() == criteria:
                    found = True
                else:
                    index += 1
            elif len(criteria) > 0:
                if self.wwnMem[index].get_name() == criteria or self.wwnMem[index].get_alias() == criteria:
                    found = True
                else:
                    index += 1
            else:
                raise ValueError('No criteria given')
        if found:
            return index
        else:
            return -1
    def del_wwn_member(self,criteria=""):
        deleted = False
        index = 0
        while not(deleted) and index < len(self.wwnMem):
            if criteria.count(":") == 7:
                if self.wwnMem[index].get_wwn() == criteria:
                    self.wwnMem.pop(index)
                    deleted = True
                else:
                    index += 1
            elif len(criteria) > 0:
                if self.wwnMem[index].get_name() == criteria or self.wwnMem[index].get_alias() == criteria:
                    self.wwnMem.pop(index)
                    deleted = True
                else:
                    index += 1
            else:
                raise ValueError('No criteria given')
    def get_all_wwn_member(self):
        return self.wwnMem
    def set_mem_alias(self):
        for mem in self.wwnMem:
            mem.set_alias("{}_{}".format(self.name,mem.get_name()))
    @classmethod
    def fromJSON(cls, data):
        wwnMem = list(map(Member.fromJSON,data["wwnMem"]))
        cls_tmp = cls(data["name"])
        for wwn in wwnMem:
            cls_tmp.add_wwn_member(wwn)
        return cls_tmp