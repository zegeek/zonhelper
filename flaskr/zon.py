from flask import (Blueprint, redirect,flash, g, render_template, request, session, url_for)
from .zonhelper import Session,Entity,Member, Zone
import json

zbp = Blueprint('zon', __name__, url_prefix='/zon')

@zbp.route('/<sessionName>')
def zon(sessionName):
    if session['session_name'] == sessionName:
        s = Session.Session(sessionName)
        g.ss = s
        zs = s.get_zones()
        display = []
        index = 0
        for z in zs:  
            dz = {
                "index" : index
            }
            member_list = []
            for i in range(len(z.zoneMem)):
                mm = {}
                if z.get_entity_type(i) == 'h':
                    mm["alias"] = s.hosts[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_alias()
                    mm["wwn"] = s.hosts[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_wwn()
                    mm["tag"] = z.zoneMem[i]
                elif z.get_entity_type(i) == 's':
                    mm["alias"] = s.storages[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_alias()
                    mm["wwn"] = s.storages[z.get_entity_index(i)].wwnMem[z.get_member_index(i)].get_wwn()
                    mm["tag"] = z.zoneMem[i]
                member_list.append(mm)
            dz['members'] = member_list
            z.set_zone_name([member_list[0]['alias'],member_list[1]['alias']])
            dz['name'] = z.name
            dz['script'] = s.__class__.cisco['zone'].format(dz['name'],dz['members'][0]['alias'],dz['members'][1]['alias'])
            display.append(dz)
            index += 1
        s.dump_zones()
        return render_template("zone.html",zones = display)
    else:
        return redirect(url_for('index'))
@zbp.route('/get/<entityType>',methods=['POST'])
def get(entityType):
    if 'session_name' in session:
        s = Session.Session(session['session_name'])
        ll = []
        index = 0
        if entityType == 'h':
            for h in s.get_hosts():
                ll.append({
                    'name': h.name,
                    'index': index
                })
                index += 1
        elif entityType == 's':
            for s in s.get_storages():
                ll.append({
                    'name': s.name,
                    'index': index
                })
                index += 1
        return json.dumps(ll)
    else:
        return "[]"

@zbp.route("/ent/<entityType>", methods=['POST'])
def ent(entityType):
    if 'session_name' in session:
        s = Session.Session(session['session_name'])
        ll = []
        data = request.get_json(force=True)
        index = int(data['index'])
        if entityType == 'h':
            i = 0
            for m in s.hosts[index].get_all_wwn_member():
                mm = {
                    'value':"{}.{}.{}".format(entityType,index,i),
                    'name': m.get_name()
                }
                ll.append(mm)
                i += 1
        elif entityType == 's':
            i = 0
            for m in s.storages[index].get_all_wwn_member():
                mm = {
                    'value':"{}.{}.{}".format(entityType,index,i),
                    'name': m.get_name()
                }
                ll.append(mm)
                i += 1
        return json.dumps(ll)
    else:
        return "[]"
@zbp.route("/add",methods=['POST'])
def add():
    if 'session_name' in session:
        data = request.get_json(force=True)
        s = Session.Session(session['session_name'])
        s.create_zone(data)
        s.dump_zones()
        return url_for('zon.zon',sessionName=session['session_name'])
    else:
        return url_for('index')

@zbp.route("/addcross",methods=['POST'])
def addcross():
    if 'session_name' in session:
        data = request.get_json(force=True)
        s = Session.Session(session['session_name'])
        members_stor = []
        members_host = []
        for d in data:
            if d[0] == 's':
                comp = d.split(Zone.Zone.separator)
                i = 0
                for mem in s.storages[int(comp[1])].get_all_wwn_member():
                    num = ""
                    for c in mem.get_name():
                        if c.isdigit():
                            num += c
                    rest = int(num) % 2 
                    if comp[2] == "odd":
                        if rest != 0:
                            members_stor.append("s.{}.{}".format(int(comp[1]),i))
                    elif comp[2] == "even":
                        if rest == 0:
                            members_stor.append("s.{}.{}".format(int(comp[1]),i))
                    i +=1
            else:
                comp = d.split(Zone.Zone.separator)
                i = 0
                for mem in s.hosts[int(comp[1])].get_all_wwn_member():
                    num = ""
                    for c in mem.get_name():
                        if c.isdigit():
                            num += c
                    rest = int(num) % 2 
                    if comp[2] == "odd":
                        if rest != 0:
                            members_host.append("h.{}.{}".format(int(comp[1]),i))
                    elif comp[2] == "even":
                        if rest == 0:
                            members_host.append("h.{}.{}".format(int(comp[1]),i))
                    i +=1
        print(members_host)
        print(members_stor)
        for m in members_host:
            for ms in members_stor:
                s.create_zone([m,ms])
        s.dump_zones()
        return url_for('zon.zon',sessionName=session['session_name'])
    else:
        return url_for('index')


@zbp.route("/del/<zoneIndex>",methods=['POST'])
def delete(zoneIndex):
    if 'session_name' in session:
        print("delete zone number {}".format(zoneIndex))
        s = Session.Session(session['session_name'])
        s.delete_zone(int(zoneIndex))
        s.dump_zones()
        return url_for('zon.zon',sessionName=session['session_name'])
    else:
        return url_for('index')